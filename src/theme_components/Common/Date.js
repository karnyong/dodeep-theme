import React, { useState, useEffect } from "react";
import { Label } from "reactstrap";

export default function Days() {
  const [currentDate, setCurrentDate] = useState("");

  useEffect(() => {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    setCurrentDate(date + "/" + month + "/" + year);
  }, []);

  return <Label>{currentDate}</Label>;
}
