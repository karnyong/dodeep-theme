import React, { useEffect } from "react";
import MetaTags from "react-meta-tags";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory, {
  PaginationProvider,
  PaginationListStandalone,
} from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import { Link } from "react-router-dom";
import * as moment from "moment";

import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Row,
  Modal,
  Badge,
  ModalHeader,
  ModalBody,
} from "reactstrap";

import { AvForm, AvField } from "availity-reactstrap-validation";

import Breadcrumbs from "../../../theme_components/Common/Breadcrumb";
import EcommerceOrdersModal from "./EcommerceOrdersModal";

import { orders } from "./ecommerce";

export default function Etable(props) {
  const [isEdit, setIsEdit] = React.useState(false);
  const [viewmodal, setViewmodal] = React.useState(false);
  const [modal, setModal] = React.useState(false);
  const [order, setOrder] = React.useState({});
  const [Orders, setOrders] = React.useState(orders);
  const [EcommerceOrderColumns] = React.useState([
    {
      text: "id",
      dataField: "id",
      sort: true,
      hidden: true,
      formatter: (cellContent, user) => <>{user.id}</>,
    },
    {
      dataField: "orderId",
      text: "Order ID",
      sort: true,
      formatter: (cellContent, row) => (
        <Link to="#" className="text-body fw-bold">
          {row.orderId}
        </Link>
      ),
    },
    {
      dataField: "billingName",
      text: "Billing Name",
      sort: true,
    },
    {
      dataField: "orderdate",
      text: "Date",
      sort: true,
      formatter: (cellContent, row) => handleValidDate(row.orderdate),
    },
    {
      dataField: "total",
      text: "Total",
      sort: true,
    },
    {
      dataField: "paymentStatus",
      text: "Payment Status",
      sort: true,
      formatter: (cellContent, row) => (
        <Badge
          className={"font-size-12 badge-soft-" + row.badgeclass}
          color={row.badgeclass}
          pill
        >
          {row.paymentStatus}
        </Badge>
      ),
    },
    {
      dataField: "paymentMethod",
      isDummyField: true,
      text: "Payment Method",
      sort: true,
      formatter: (cellContent, row) => (
        <>
          <i
            className={
              row.paymentMethod !== "COD"
                ? "fab fa-cc-" + toLowerCase1(row.paymentMethod) + " me-1"
                : "fab fas fa-money-bill-alt me-1"
            }
          />{" "}
          {row.paymentMethod}
        </>
      ),
    },
    {
      dataField: "view",
      isDummyField: true,
      text: "View Details",
      sort: true,
      formatter: () => (
        <Button
          type="button"
          color="primary"
          className="btn-sm btn-rounded"
          onClick={toggleViewModal}
        >
          View Details
        </Button>
      ),
    },
    {
      dataField: "action",
      isDummyField: true,
      text: "Action",
      formatter: (cellContent, order) => (
        <>
          <div className="d-flex gap-3">
            <Link to="#" className="text-success">
              <i
                className="mdi mdi-pencil font-size-18"
                id="edittooltip"
                onClick={() => handleOrderClick(order)}
              />
            </Link>
            <Link to="#" className="text-danger">
              <i
                className="mdi mdi-delete font-size-18"
                id="deletetooltip"
                onClick={() => handleDeleteOrder(order)}
              />
            </Link>
          </div>
        </>
      ),
    },
  ]);

  const toLowerCase1 = (str) => {
    return str.toLowerCase();
  };

  useEffect(() => {
  }, []);

  const toggle = () => {
    setModal(!modal);
  };

  const handleOrderClicks = () => {
    setOrder("");
    setIsEdit(false);
    toggle();
  };

  // eslint-disable-next-line no-unused-vars
  const handleTableChange = (type, { page, searchText }) => {
    const { Orders } = props;
    setOrders({
      Orders: Orders.filter((order) =>
        Object.keys(order).some(
          (key) =>
            typeof order[key] === "string" &&
            order[key].toLowerCase().includes(searchText.toLowerCase())
        )
      ),
    });
  };

  const toggleViewModal = () => {
    setViewmodal(!viewmodal);
  };

  /* Insert,Update Delete data */

  const handleDeleteOrder = (deleteOrder) => {
    if (deleteOrder.id !== undefined) {
      setOrders(
        Orders.filter(
          (order) => order.id.toString() !== deleteOrder.id.toString()
        )
      );
      // onPaginationPageChange(1);
    }
  };

  const handleOrderClick = (arg) => {
    const order = arg;
    setOrder({
      id: order.id,
      orderId: order.orderId,
      billingName: order.billingName,
      orderdate: order.orderdate,
      total: order.total,
      paymentStatus: order.paymentStatus,
      paymentMethod: order.paymentMethod,
      badgeclass: order.badgeclass,
    });

    setIsEdit(true);

    toggle();
  };

  /**
   * Handling submit Order on Order form
   */
  const handleValidOrderSubmit = (e, values) => {
    // const { isEdit, order } = this.state;

    if (isEdit) {
      const updateOrder = {
        id: order.id,
        orderId: values.orderId,
        billingName: values.billingName,
        orderdate: values.orderdate,
        total: values.total,
        paymentStatus: values.paymentStatus,
        paymentMethod: values.paymentMethod,
        badgeclass: values.badgeclass,
      };

      console.log(updateOrder);

      // update Order
      setOrders(
        Orders.map((order) =>
          order.id.toString() === updateOrder.id.toString()
            ? { order, ...updateOrder }
            : order
        )
      );
    } else {
      const newOrder = {
        id: Math.floor(Math.random() * (30 - 20)) + 20,
        orderId: values["orderId"],
        billingName: values["billingName"],
        orderdate: values["orderdate"],
        total: values["total"],
        paymentStatus: values["paymentStatus"],
        paymentMethod: values["paymentMethod"],
        badgeclass: values["badgeclass"],
      };

      console.log(newOrder);
      console.log(Orders);

      // save new Order
      setOrders([...Orders, newOrder]);
    }
    toggle();
  };

  const handleValidDate = (date) => {
    const date1 = moment(new Date(date)).format("DD MMM Y");
    return date1;
  };

  const { SearchBar } = Search;

  // const { isEdit } = isEdit;

  //pagination customization
  const pageOptions = {
    sizePerPage: 10,
    totalSize: Orders.length, // replace later with size(Order),
    custom: true,
  };

  const defaultSorted = [
    {
      dataField: "orderId",
      order: "desc",
    },
  ];

  const selectRow = {
    mode: "checkbox",
  };

  return (
    <React.Fragment>
      <EcommerceOrdersModal isOpen={viewmodal} toggle={toggleViewModal} />
      <div className="page-content">
        <MetaTags>
          <title>Orders | Skote - React Admin & Dashboard Template</title>
        </MetaTags>
        <Container fluid>
          <Breadcrumbs title="Ecommerce" breadcrumbItem="Orders" />
          <Row>
            <Col xs="12">
              <Card>
                <CardBody>
                  <PaginationProvider
                    pagination={paginationFactory(pageOptions || [])}
                    keyField="id"
                    columns={EcommerceOrderColumns || []}
                    data={Orders || []}
                  >
                    {({ paginationProps, paginationTableProps }) => (
                      <ToolkitProvider
                        keyField="id"
                        data={Orders}
                        columns={EcommerceOrderColumns || []}
                        bootstrap4
                        search
                      >
                        {(toolkitProps) => (
                          <React.Fragment>
                            <Row className="mb-2">
                              <Col sm="4">
                                <div className="search-box me-2 mb-2 d-inline-block">
                                  <div className="position-relative">
                                    <SearchBar {...toolkitProps.searchProps} />
                                    <i className="bx bx-search-alt search-icon" />
                                  </div>
                                </div>
                              </Col>
                              <Col sm="8">
                                <div className="text-sm-end">
                                  <Button
                                    type="button"
                                    color="success"
                                    className="btn-rounded mb-2 me-2"
                                    onClick={handleOrderClicks}
                                  >
                                    <i className="mdi mdi-plus me-1" /> Add New
                                    Order
                                  </Button>
                                </div>
                              </Col>
                            </Row>
                            <div className="table-responsive">
                              <BootstrapTable
                                {...toolkitProps.baseProps}
                                {...paginationTableProps}
                                responsive
                                defaultSorted={defaultSorted}
                                bordered={false}
                                striped={false}
                                selectRow={selectRow}
                                classes={
                                  "table align-middle table-nowrap table-check"
                                }
                                headerWrapperClasses={"table-light"}
                              />
                              <Modal isOpen={modal} className={props.className}>
                                <ModalHeader toggle={toggle} tag="h4">
                                  {!!isEdit ? "Edit Order" : "Add Order"}
                                </ModalHeader>
                                <ModalBody>
                                  <AvForm
                                    onValidSubmit={handleValidOrderSubmit}
                                  >
                                    <Row form>
                                      <Col className="col-12">
                                        <div className="mb-3">
                                          <AvField
                                            name="orderId"
                                            label="Order Id"
                                            type="text"
                                            errorMessage="Invalid orderId"
                                            validate={{
                                              required: { value: true },
                                            }}
                                            value={order.orderId || ""}
                                          />
                                        </div>
                                        <div className="mb-3">
                                          <AvField
                                            name="billingName"
                                            label="Billing Name"
                                            type="text"
                                            errorMessage="Invalid Billing Name"
                                            validate={{
                                              required: { value: true },
                                            }}
                                            value={order.billingName || ""}
                                          />
                                        </div>
                                        <div className="mb-3">
                                          <AvField
                                            name="orderdate"
                                            label="Date"
                                            type="date"
                                            errorMessage="Invalid Date"
                                            validate={{
                                              required: { value: true },
                                            }}
                                            value={order.orderdate || ""}
                                          />
                                        </div>
                                        <div className="mb-3">
                                          <AvField
                                            name="total"
                                            label="Total"
                                            type="text"
                                            errorMessage="Invalid Total"
                                            validate={{
                                              required: { value: true },
                                            }}
                                            value={order.total || ""}
                                          />
                                        </div>
                                        <div className="mb-3">
                                          <AvField
                                            name="paymentStatus"
                                            label="Payment Status"
                                            type="select"
                                            id="status1"
                                            className="form-select"
                                            errorMessage="Invalid Payment Status"
                                            validate={{
                                              required: { value: true },
                                            }}
                                            value={
                                              order.paymentStatus || "Paid"
                                            }
                                          >
                                            <option>Paid</option>
                                            <option>Chargeback</option>
                                            <option>Refund</option>
                                          </AvField>
                                        </div>
                                        <div className="mb-3">
                                          <AvField
                                            name="badgeclass"
                                            label="Badge Class"
                                            type="select"
                                            className="form-select"
                                            errorMessage="Invalid Badge Class"
                                            validate={{
                                              required: { value: true },
                                            }}
                                            value={
                                              order.badgeclass || "success"
                                            }
                                          >
                                            <option>success</option>
                                            <option>danger</option>
                                            <option>warning</option>
                                          </AvField>
                                        </div>
                                        <div className="mb-3">
                                          <AvField
                                            name="paymentMethod"
                                            label="Payment Method"
                                            type="select"
                                            className="form-select"
                                            errorMessage="Invalid Payment Method"
                                            validate={{
                                              required: { value: true },
                                            }}
                                            value={
                                              order.paymentMethod ||
                                              "Mastercard"
                                            }
                                          >
                                            <option>Mastercard</option>
                                            <option>Visa</option>
                                            <option>Paypal</option>
                                            <option>COD</option>
                                          </AvField>
                                        </div>
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col>
                                        <div className="text-end">
                                          <button
                                            type="submit"
                                            className="btn btn-success save-user"
                                          >
                                            Save
                                          </button>
                                        </div>
                                      </Col>
                                    </Row>
                                  </AvForm>
                                </ModalBody>
                              </Modal>
                            </div>
                            <div className="pagination pagination-rounded justify-content-end mb-2">
                              <PaginationListStandalone {...paginationProps} />
                            </div>
                          </React.Fragment>
                        )}
                      </ToolkitProvider>
                    )}
                  </PaginationProvider>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
}